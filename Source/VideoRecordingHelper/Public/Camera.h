// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SlateMaterialBrush.h"
#include "GameFramework/Actor.h"
#include "Camera.generated.h"

UCLASS()
class VIDEORECORDINGHELPER_API ACamera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACamera();

	UPROPERTY(EditAnywhere) FVector2D RenderSize = {1920,1080};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY() UStaticMeshComponent* CameraMesh;
	UPROPERTY() USceneCaptureComponent2D* SceneCapture;
	UPROPERTY() UTextureRenderTarget2D* RenderTarget;
	UPROPERTY() UMaterial* CameraDisplayMaterial;
	UPROPERTY() UMaterialInstanceDynamic* CameraDisplayMaterialDynamic;
	UPROPERTY() UMaterial* WindowDisplayMaterial;
	UPROPERTY() UMaterialInstanceDynamic* WindowDisplayMaterialDynamic;
	FSlateMaterialBrush* WindowBrush;


	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void CreateWindow();

	TSharedPtr<SWindow> Window = nullptr;
	TSharedPtr<SImage> Image = nullptr;
};
